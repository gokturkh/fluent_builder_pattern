package com.hg.fluentbuilder;

import com.hg.fluentbuilder.interfaces.*;

public final class Email {

    private Email(String to, String from, String subject, String content, String bcc, String cc) {
    }

    public static class EmailBuilder implements EmailFrom, EmailTo, EmailSubject, EmailContent, EmailCreator {

        private String from;
        private String to;
        private String subject;
        private String content;
        private String cc;
        private String bcc;

        private EmailBuilder() {
        }

        public static EmailFrom getInstance() {
            return new EmailBuilder();
        }

        @Override
        public EmailTo setFrom(String from) {
            this.from = from;
            return this;
        }

        @Override
        public EmailSubject setTo(String to) {
            this.to = to;
            return this;
        }

        @Override
        public EmailContent setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        @Override
        public EmailCreator setContent(String content) {
            this.content = content;
            return this;
        }

        @Override
        public EmailCreator setCC(String cc) {
            this.cc = cc;
            return this;
        }

        @Override
        public EmailCreator setBCC(String bcc) {
            this.bcc = bcc;
            return this;
        }

        @Override
        public Email build() {
            return new Email(from, to, subject, content, cc, bcc);
        }

        @Override
        public String toString() {
            return "EmailBuilder{" +
                    "from='" + from + '\'' +
                    ", to='" + to + '\'' +
                    ", subject='" + subject + '\'' +
                    ", content='" + content + '\'' +
                    ", cc='" + cc + '\'' +
                    ", bcc='" + bcc + '\'' +
                    '}';
        }
    }
}
