package com.hg.fluentbuilder;

public class Main {

    public static void main(String[] args) {
        Email email = Email.EmailBuilder
                .getInstance()
                .setFrom("from")
                .setTo("to")
                .setSubject("subject")
                .setContent("content")
                .build();
    }
}
