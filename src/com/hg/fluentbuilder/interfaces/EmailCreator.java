package com.hg.fluentbuilder.interfaces;

import com.hg.fluentbuilder.Email;

public interface EmailCreator {
    EmailCreator setCC(String cc);

    EmailCreator setBCC(String bcc);

    Email build();
}
