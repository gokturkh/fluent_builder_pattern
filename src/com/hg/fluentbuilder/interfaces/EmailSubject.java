package com.hg.fluentbuilder.interfaces;

public interface EmailSubject {
    EmailContent setSubject(String subject);
}
