package com.hg.fluentbuilder.interfaces;

public interface EmailTo {
    EmailSubject setTo(String to);
}
