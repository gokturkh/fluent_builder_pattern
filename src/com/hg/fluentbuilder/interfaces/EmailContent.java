package com.hg.fluentbuilder.interfaces;

public interface EmailContent {
    EmailCreator setContent(String content);
}
