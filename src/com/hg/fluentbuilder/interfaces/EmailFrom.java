package com.hg.fluentbuilder.interfaces;

public interface EmailFrom {
    EmailTo setFrom(String from);
}
